function validate_email(email) {
    var email_error = $("#email_error");
    $.post('ajax.php', { email: email }, function(data) {
        email_error.text(data);
    })
      .fail(function() {
        alert( "error" );
    });
}

function email_password_validation() {
    var error = 0;
    var email = $("#email").val();
    var email_error = $("#email_error");
    var password = $("#password").val();
    var password_error = $("#password_error");
    var confirm_password = $("#cpassword").val();
    var cpassword_error = $("#cpassword_error");
    var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var password_format = /^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/;
    if (!mail_format.test(email)) {
        error = 1;
        if ("" === email) {
            email_error.text("This field is required.");
        } else {
            email_error.text("Invalid format.");
        }
    } else {
        email_error.text("");
    }
    if (!password_format.test(password)) {
         error = 1;
        if ("" === password) {
            password_error.text("This field is required.");
        } else {
            password_error.text(
                "Password must be atleast 8 letters with atleast 1 digit."
            );
        }
    } else {
        password_error.text("");
    }
    if (!password_format.test(confirm_password)) {
        error = 1;
        if ("" === confirm_password) {
            cpassword_error.text("This field is required.");
        } else {
            cpassword_error.text(
                "Password must be atleast 8 letters with atleast 1 digit."
            );
        }
    } else if (confirm_password != password) {
        error = 1;
        cpassword_error.text("Passwod does'nt match!");
    } else {
        cpassword_error.text("");
    }
    return !error;
}

function account_info_validation() {
    var error = 0;
    var fname = $("#fname").val();
    var fname_error = $("#fname_error");
    var mname = $("#mname").val();
    var mname_error = $("#mname_error");
    var lname = $("#lname").val();
    var lname_error = $("#lname_error");
    var male = $("#male");
    var female = $("#female");
    var gender_error = $("#gender_error");
    var name_format = /^[a-zA-Z]*$/;
    if ("" === fname) {
        error = 1;
        fname_error.text("This field is required.");
    } else if (!name_format.test(fname)) {
        error = 1;
        fname_error.text("Invalid format.");
    } else {
        fname_error.text("");
    }
    if (mname != "") {
        if (!name_format.test(mname)) {
            error = 1;
            mname_error.text("Invalid format.");
        } else {
            mname_error.text("");
        }
    }
    if ("" === lname) {
        error = 1;
        lname_error.text("This field is required.");
    } else if (!name_format.test(lname)) {
        error = 1;
        lname_error.text("Invalid format.");
    } else {
        lname_error.text("");
    }
    if (!($("input[name='gender']:checked").val())) {
        error = 1;
        gender_error.text("Select your gender.");
    } else {
        gender_error.text("");
    }
    return !error;
}

function contact_details_validation() {
    var error = 0;
    var address = $("#address").val();
    var address_error = $("#address_error");
    var city = $("#city").val();
    var city_error = $("#city_error");
    var state = $("#states").val();
    var state_error = $("#state_error");
    var pincode = $("#pincode").val();
    var pincode_error = $("#pincode_error");
    var country = $("#country").val();
    var country_error = $("#country_error");
    var phoneno = $("#phoneno").val();
    var phoneno_error = $("#phoneno_error");
    var pincode_format = /^[0-9]{6}$/;
    var phone_format = /^[0-9]{10}$/;
    if ("" === address) {
        error = 1;
        address_error.text("This field is required.");
    } else {
        address_error.text("");
    }
    if ("" === city) {
        error = 1;
        city_error.text("This field is required.");
    } else {
        city_error.text("");
    }
    if ("" === state) {
        error = 1;
        state_error.text("This field is required.");
    } else {
        state_error.text("");
    }
    if ("" === pincode) {
        error = 1;
        pincode_error.text("This field is required.");
    } else if (!pincode_format.test(pincode)) {
        error = 1;
        pincode_error.text("Invalid format.");
    } else {
        pincode_error.text("");
    }
    if ("" === country) {
        error = 1;
        country_error.text("This field is required.");
    } else {
        country_error.text("");
    }
    if ("" === phoneno) {
        error = 1;
        phoneno_error.text("This field is required.");
    } else if (!phone_format.test(phoneno)) {
        error = 1;
        phoneno_error.text("Invalid format.");
    } else {
        phoneno_error.text("");
    }
    return !error;
}

$(document).ready(function() {
    $("#email").blur(function(event) {
        var email = $("#email");
        var email_error = $("#email_error");
        var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if("" === email.val()){
            email.focus();
            email.css("border", "solid 1px red");
            email_error.text("This field is required.");
        } else {
            if(!mail_format.test(email.val())) {
                email.focus();
                email.css("border", "solid 1px red");
                email_error.text("Invalid format.");
            } else {
                validate_email(email.val());     
            }
        }
    });
    
    $("#atag").click(function(event) {
        if(!email_password_validation()) {
            event.stopPropagation();
        } else {
            $("#tab2").addClass("active");
            $("#tab1").removeClass("active");
        }
    });

    $("#previous1").click(function() {
        $("#tab1").addClass("active");
        $("#tab2").removeClass("active");
    });

    $("#acc_tab").click(function(event) {
        if(!email_password_validation()) {
            event.stopPropagation();
        }
    });

    $("#next2").click(function(event) {
        if(!account_info_validation()) {
            event.stopPropagation();
        } else {
            $("#tab3").addClass("active");
            $("#tab2").removeClass("active");
        }
    });

    $("#previous2").click(function() {
        $("#tab2").addClass("active");
        $("#tab3").removeClass("active");
    });

    $("#contact").click(function(event) {
        if(!email_password_validation()) {
            event.stopPropagation();
        }else if(!account_info_validation()) {
            event.stopPropagation();
        }
    });

    $("#next3").click(function(event) {
        if(!contact_details_validation()) {
            event.stopPropagation();
        } else {
            $("#tab4").addClass("active");
            $("#tab3").removeClass("active");
        }
    });

    $("#previous3").click(function() {
        $("#tab3").addClass("active");
        $("#tab4").removeClass("active");
    });

    $("#agree").click(function(event) {
        if(!email_password_validation()) {
            event.stopPropagation();
        } else if(!account_info_validation()) {
            event.stopPropagation();
        } else if(!contact_details_validation()) {
            event.stopPropagation();
        }
    });

    $("#i_agree").click(function() {
        if ($("input[name='iagree']:checked").val()) {
            $("#submit").prop('disabled', false);
        } else {
            $("#submit").prop('disabled', true);
        }
    });
    
});