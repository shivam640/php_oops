function email_validation() {
    var error =0;
    var email = $("#email");
    var email_error = $("#email_error");
    var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(!mail_format.test(email.val())) {
        error = 1;
        email.focus();
        if("" === email.val()){
            email_error.text("This field is required.");
        } else {
            email_error.text("Invalid format.");     
        }
    } else {
        email_error.text("");
    }
    return !error;
}

function password_validation() {
    var error =0;
    var password = $("#password");
    var password_error = $("#password_error");
    if("" === password.val()){
        error = 1;
        password.focus();
        password_error.text("This field is required.");
    } else {
        password_error.text("");
    }
    return !error;
}

$(document).ready(function() {
    $("#email").blur(function(event) {
        email_validation();
    });
    
    $("#password").blur(function() {
        password_validation();
    });
    
    $("#login").click(function(event) {
        if(!email_validation() || !password_validation()) {
            event.preventDefault();
        }
    });
});