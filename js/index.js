function fname_validation() {
    var error = 0;
    var fname = $("#fname").val();
    var fname_error = $("#fname_error");
    var name_format = /^[a-zA-Z]*$/;
    if ("" === fname) {
        error = 1;
        fname_error.text("This field is required.");
    } else if (!name_format.test(fname)) {
        error = 1;
        fname_error.text("First name can contain only letters.");
    } else {
        fname_error.text("");
    }
    return !error;
}

function mname_validation() {
    var error = 0;
    var mname = $("#mname").val();
    var mname_error = $("#mname_error");
    var name_format = /^[a-zA-Z]*$/;
    if (mname != "") {
        if (!name_format.test(mname)) {
            error = 1;
            mname_error.text("Middle name can contain only letters.");
        } else {
            mname_error.text("");
        }
    }
    return !error;
}

function lname_validation() {
    var error = 0;
    var lname = $("#lname").val();
    var lname_error = $("#lname_error");
    var name_format = /^[a-zA-Z]*$/;
    if ("" === lname) {
        error = 1;
        lname_error.text("This field is required.");
    } else if (!name_format.test(lname)) {
        error = 1;
        lname_error.text("Last name can contain only letters.");
    } else {
        lname_error.text("");
    }
    return !error;
}

function address_validation() {
    var error = 0;
    var address = $("#address").val();
    var address_error = $("#address_error");
    if ("" === address) {
        error = 1;
        address_error.text("This field is required.");
    } else {
        address_error.text("");
    }
    return !error;
}

function city_validation() {
    var error = 0;
    var city = $("#city").val();
    var city_error = $("#city_error");
    if ("" === city) {
        error = 1;
        city_error.text("This field is required.");
    } else {
        city_error.text("");
    }
    return !error;
}

function states_validation() {
    var error = 0;
    var state = $("#states").val();
    var state_error = $("#states_error");
    if ("" === state) {
        error = 1;
        state_error.text("This field is required.");
    } else {
        state_error.text("");
    }
    return !error;
}

function pincode_validation() {
    var error = 0;
    var pincode_format = /^[0-9]{6}$/;
    var pincode = $("#pincode").val();
    var pincode_error = $("#pincode_error");
    if ("" === pincode) {
        error = 1;
        pincode_error.text("This field is required.");
    } else if (!pincode_format.test(pincode)) {
        error = 1;
        pincode_error.text("Pin code can be 6 digit only");
    } else {
        pincode_error.text("");
    }
    return !error;
}

function country_validation() {
    var error = 0;
    var country = $("#country").val();
    var country_error = $("#country_error");
    if ("" === country) {
        error = 1;
        country_error.text("This field is required.");
    } else {
        country_error.text("");
    }
    return !error;
}

function phone_validation() {
    var error = 0;
    var phone_format = /^[0-9]{10}$/;
    var phoneno = $("#phone").val();
    var phone_error = $("#phone_error");
    if ("" === phoneno) {
        error = 1;
        phone_error.text("This field is required.");
    } else if (!phone_format.test(phoneno)) {
        error = 1;
        phone_error.text("Phone no. can be 10 digits only.");
    } else {
        phone_error.text("");
    }
    return !error;
}

$(document).ready(function() {
    $("#edit").click(function() {
        $("#tab2").addClass("active");
        $("#tab1").removeClass("active");
    });
    
    $("#fname").blur(function() {
        fname_validation();
    });
    
    $("#mname").blur(function() {
        mname_validation();
    });
    
    $("#lname").blur(function() {
        lname_validation();
    });
    
    $("#address").blur(function() {
        address_validation();
    });
    
    $("#city").blur(function() {
        city_validation();
    });
    
    $("#states").blur(function() {
        states_validation();
    });
    
    $("#pincode").blur(function() {
        pincode_validation();
    });
    
    $("#country").blur(function() {
        country_validation();
    });
    
    $("#phone").blur(function() {
        phone_validation();
    });
    
    $("#submit").click(function(event) {
        if (!fname_validation() || !mname_validation() || !lname_validation()
            || !address_validation() || !city_validation()
            || !states_validation() || !pincode_validation()
            || !country_validation() || !phone_validation()
           ) {
            event.preventDefault();
        }
    }); 
});
