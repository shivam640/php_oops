<?php
require_once("resources/constant.php");
require_once("resources/config.php");

/**
 * Class Validator
 */
class Validator
{
    private $fields = array();
    private $field_errors = array();
    private $is_form_valid = true;

    /**
     * Adds fields to array.
     *
     * @param $field_name
     */
    public function add_field($field_name)
    {
        $this->fields[] = $field_name;
        $this->$field_errors[$field_name] = array();
    }

    /**
     * Adds rule to fields.
     *
     * @param $field_name
     * @param $field_rule
     */
    public function add_rule_to_field($field_name, $field_rule)
    {
        $rule_name = $field_rule[0];
        switch ($rule_name) {
            case 'empty':
                if (strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        "Can not be empty."
                    );
                }
                break;
            case 'select':
                if (strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        "Please select."
                    );
                }
                break;
            case 'email_format':
                if (!filter_var($_POST[$field_name], FILTER_VALIDATE_EMAIL) 
                    && !strlen($_POST[$field_name]) == 0
                ) {
                    $this->add_error_to_field(
                        $field_name,
                        "Invalid email id."
                    );
                }
                break;
            case 'password_format':
                if (!preg_match(password_expression, $_POST[$field_name])
                    && !strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        "Password must be 8 characters long, contain atleast
                        1 letter and 1 digit!"
                    );
                }
                break;
            case 'compare':
                if (strcmp($_POST[$field_name],$field_rule[1])
                    && !strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        "Password did'nt match!"
                    );
                }
                break;
            case 'name_format':
                if (!preg_match(name_expression, $_POST[$field_name])
                   && !strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        "Only letters allowed!"
                    );
                }
                break;
            case 'pcode':
                if (!preg_match(pcode_expression, $_POST[$field_name])
                    && !strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        'Must be 6 digits only'
                    );
                }
                break;
            case 'phone':
                if (!preg_match(phone_expression,$_POST['phoneno'])
                    && !strlen($_POST[$field_name]) == 0) {
                    $this->add_error_to_field(
                        $field_name,
                        'Must be 10 digits only'
                    );
                }
                break;
            default:
                break;  
        }
    }

    /**
     * Adds error to fields.
     *
     * @param $field_name
     * @param $error_message
     */
    private function add_error_to_field($field_name, $error_message)
    {
        $this->is_form_valid = false;
        $this->field_errors[$field_name][] = $error_message;
    }

    /**
     * Prints error of the correcponding fields.
     *
     * @param $field_name
     */
    public function out_field_error($field_name)
    {
        if (isset($this->field_errors[$field_name])) {
            foreach ($this->field_errors[$field_name] as $field_error) {
                echo "<span class='label label-danger'>$field_error</span>";
            }
        }
    }

    /**
     * Checks the error.
     *
     * @return bool
     */
    public function form_valid()
    {
        return $this->is_form_valid;
    }
}
        