<?php

/**
 * Class Connection
 */
class Connection
{
    public $conn;
    public function __construct($DB_HOST, $DB_USERNAME, $DB_PASSWORD, $DB_NAME)
    {
        $this->connect($DB_HOST, $DB_USERNAME, $DB_PASSWORD, $DB_NAME);
    }

    /**
     * Establishing the connections to the database using Mysqli.
     *
     * @param $DB_HOST
     * @param $DB_USERNAME
     * @param $DB_PASSWORD
     * @param $DB_NAME
     * @return bool
     */
    public function connect($DB_HOST, $DB_USERNAME, $DB_PASSWORD, $DB_NAME)
    {
        $this->conn = mysqli_connect(
            $DB_HOST,
            $DB_USERNAME,
            $DB_PASSWORD,
            $DB_NAME
        );
        if ($this->conn) {
            return true;
        }
    }

    /**
     * This method inserts data into db.
     *
     * @param $data
     * @return bool
     */
    public function insert($data)
    {
        /** Creating sql statement using implode function. */
        $string = "insert into user (";
        $string .= implode(",", array_keys($data)) . ') values (';
        $string .= "'" . implode("','", array_values($data)) . "')";
        if (mysqli_query($this->conn, $string)) {
            return true;
        } else {
            echo mysqli_error($this->conn);
        }
    }

    /**
     * Closing the connection.
     */
    public function disconnect()
    {
        mysqli_close($this->conn);
    }
    public function verify_email($email, $token)
    {
        $sql = mysqli_query($this->conn, "select id from user where email='"
            .$email."' && verification_code='"
            .$token."' && is_email_confirmed=0");
        if ($sql->num_rows > 0) {
           mysqli_query($this->conn, "update user set is_email_confirmed=1,
           verification_code='' where email='" . $email . "'");
           return true;
       }
    }

    /**
     * Authenticating user credentials while login.
     *
     * @param $email_address
     * @param $password
     * @return string
     */
    public function authenticate($email_address, $password)
    {
        $sql = mysqli_query($this->conn, "select id, password, 
        is_email_confirmed from user where email='" . $email_address . "'");
        if ($sql->num_rows > 0) {
            $data = $sql->fetch_array();
            if (password_verify($password, $data['password'])) {
                if ($data['is_email_confirmed'] == 0) {
                    return 'Please verify your email';
                } else {
                    session_start();
                    $_SESSION['id'] = $data['id'];
                    header('Location: index.php');
                }
            } else {
                return 'incorrect password';
            }
        } else {
            return 'invalid email';
        }
    }

    /**
     * Retriving data from the database.
     *
     * @return mixed
     */
    public function get_data()
    {
        session_start();
        $sql = mysqli_query($this->conn, "select email, fname, mname, lname,
        gender, address,city, state, pincode, country, phone from user where id
        = " . $_SESSION['id']);
        if ($sql->num_rows > 0) {
            return $sql->fetch_array();
        }  
    }

    /**
     * Updating data of user.
     *
     * @return bool
     */
    public function change_data()
    {
        $fname = mysqli_real_escape_string($this->conn, $_POST['fname']);
        $mname = mysqli_real_escape_string($this->conn, $_POST['mname']);
        $lname = mysqli_real_escape_string($this->conn, $_POST['lname']);
        $address = mysqli_real_escape_string($this->conn, $_POST['address']);
        $city = mysqli_real_escape_string($this->conn, $_POST['city']);
        $state = mysqli_real_escape_string($this->conn, $_POST['state']);
        $pcode = mysqli_real_escape_string($this->conn, $_POST['pcode']);
        $country = mysqli_real_escape_string($this->conn, $_POST['country']);
        $phoneno = mysqli_real_escape_string($this->conn, $_POST['phoneno']);
        $string = "update user set fname='" . $fname . "', mname='"
                    . $mname . "', lname='" . $lname . "', gender="
                    . $_POST['gender'] . ", address='" . $address . "',city='"
                    . $city . "', state='" . $state . "', pincode="
                    . $pcode . ", country='" . $country . "', phone="
                    . $phoneno . " where id=" . $_SESSION['id'];
        if (mysqli_query($this->conn, $string)) {
            return true;
        }
    }

    /**
     * Checking for email.
     *
     * @param $email
     * @return bool
     */
    public function check($email)
    {
        $sql = mysqli_query($this->conn, "select email from user where email='"
            . $email . "'");
        if ($sql->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }
}
