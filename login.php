<?php
require_once("resources/library/validator.php");
require_once("resources/library/connection.php");
require_once("resources/config.php");

session_start();
if (isset($_SESSION['id'])){
    header('Location: index.php');
    exit();
 }

/**
 * Validating form.
 *
 * @param $validator
 * @return bool
 */
function login_validation($validator)
{
    if ($_POST) {
        $validator->add_field('email_address');
        $validator->add_rule_to_field('email_address', array('empty'));
        $validator->add_field('password');
        $validator->add_rule_to_field('password', array('empty'));
        if ($validator->form_valid()) {
            return true;
        }
    }
}
 
$validator = new Validator;
if (login_validation($validator)) {
    /**
     * Connecting to db.
     */
    $connection = new Connection(
        $DB_HOST, 
        $DB_USERNAME, 
        $DB_PASSWORD, 
        $DB_NAME
    );

    $email_address = mysqli_real_escape_string($connection->conn, $_POST['email_address']);
    $password = mysqli_real_escape_string($connection->conn, $_POST['password']);

    $message = $connection->authenticate(
        $email_address,
        $password
    );
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>login page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/login.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My page</a>
                </div>
                <div class="collapse navbar-collapse" id="navcollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="signup1.php">
                                <span class="glyphicon glyphicon-log-in">
                                </span> Sign Up
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php
            if ($_GET['registered'] == 'true') {
                echo '<div class="alert alert-success">Successfully registered!
                    A mail has been sent to your email account. Please verify
                    to login.</div>';
            }
        ?>
        <form action="login.php" method="post">
            <div id="page">
                <div class="container">
                    <div class="row">
                        <div class=
                                "col-md-5 col-md-offset-3 panel panel-default">
                            <div class="panel-body text-center" id="text">
                                <h2> log in to my page</h2>
                                <div class="form-group">
                                <input type="text" 
                                       class="form-control"
                                       placeholder="Email Address"
                                       name="email_address" id="email"
                                       value="<?php
                                        if(isset($_POST['email_address']))
                                            echo $_POST['email_address']; ?>">
                                    <div class='label label-danger' 
                                             id="email_error"></div>
                                <?php
                                    $validator->out_field_error(
                                        'email_address'
                                    );
                                ?>
                                </div>
                                <div class="form-group">
                                <input type= "password" id= "password"
                                    class="form-control" placeholder= "Password"
                                    name="password"
                                    value="<?php if(isset($_POST['password']))
                                                echo $_POST['password']; ?>">
                                    <div class='label label-danger' 
                                             id="password_error"></div>
                                <?php
                                    $validator->out_field_error('password');
                                ?>
                                </div>
                                <button type="submit" name="login" id="login"
                                        class="btn btn-primary form-control">
                                    <span class="glyphicon glyphicon-log-in">
                                    </span> Log In
                                </button>
                                <br>
                                <a href="forgetpassword.php">Forgotten account?
                                </a>
                                &nbsp;&nbsp;
                                <a href="signup1.php">Sign up for my page</a>
                                <br>
                                <?php if(isset($message)) {echo '<span class=
                                "label label-danger">' . $message . '</span>';}
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous">
        </script>
        <script src="js/login.js"></script>
    </body>
</html>