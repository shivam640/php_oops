<?php
require_once("resources/library/validator.php");
require_once("resources/library/connection.php");
require_once("resources/config.php");
require_once("resources/constant.php");
require_once('mail.php');

session_start();
if (isset($_SESSION['id'])) {
    header('Location: index.php');
}

/**
 * Validating the form.
 *
 * @param $validator
 * @return bool
 */
function validation($validator)
{
    if ($_POST) {
        $validator->add_field('email_address');
        $validator->add_rule_to_field('email_address', array('empty'));
        $validator->add_rule_to_field('email_address', array('email_format'));
        $validator->add_field('password');
        $validator->add_rule_to_field('password', array('empty'));
        $validator->add_rule_to_field('password', array('password_format'));
        $validator->add_field('confirm_password');
        $validator->add_rule_to_field('confirm_password', array('empty'));
        $validator->add_rule_to_field(
            'confirm_password',
            array('password_format')
        );
        $validator->add_rule_to_field(
            'confirm_password',
            array('compare', $_POST['password'])
        );
        $validator->add_field('fname');
        $validator->add_rule_to_field('fname', array('empty'));
        $validator->add_rule_to_field('fname', array('name_format'));
        $validator->add_field('mname');
        $validator->add_rule_to_field('mname', array('name_format'));
        $validator->add_field('lname');
        $validator->add_rule_to_field('lname', array('empty'));
        $validator->add_rule_to_field('lname', array('name_format'));
        $validator->add_field('gender');
        $validator->add_rule_to_field('gender', array('select'));
        $validator->add_field('address');
        $validator->add_rule_to_field('address', array('empty'));
        $validator->add_field('city');
        $validator->add_rule_to_field('city', array('empty'));
        $validator->add_rule_to_field('city', array('name_format'));
        $validator->add_field('state');
        $validator->add_rule_to_field('state', array('empty'));
        $validator->add_field('pcode');
        $validator->add_rule_to_field('pcode', array('empty'));
        $validator->add_rule_to_field('pcode', array('pcode'));
        $validator->add_field('country');
        $validator->add_rule_to_field('country', array('empty'));
        $validator->add_rule_to_field('country', array('name_format'));
        $validator->add_field('phoneno');
        $validator->add_rule_to_field('phoneno', array('empty'));
        $validator->add_rule_to_field('phoneno', array('phone'));
        $validator->add_field('iagree');
        $validator->add_rule_to_field('iagree', array('select'));
        if ($validator->form_valid()) {
            return true;
        }
    }
}

$validator = new Validator;
if (validation($validator)) {
    /**
     * Connecting to db.
     */
    $connection = new Connection(
        $DB_HOST, 
        $DB_USERNAME, 
        $DB_PASSWORD, 
        $DB_NAME
    );
    
    /** Generating token */
    for ($i = 0; $i < 20; $i++) {
        $token .= text[mt_rand(0,61)];
    }

    $email_address = mysqli_real_escape_string($connection->conn, $_POST['email_address']);
    $password = mysqli_real_escape_string($connection->conn, $_POST['password']);
    $fname = mysqli_real_escape_string($connection->conn, $_POST['fname']);
    $mname = mysqli_real_escape_string($connection->conn, $_POST['mname']);
    $lname = mysqli_real_escape_string($connection->conn, $_POST['lname']);
    $address = mysqli_real_escape_string($connection->conn, $_POST['address']);
    $city = mysqli_real_escape_string($connection->conn, $_POST['city']);
    $state = mysqli_real_escape_string($connection->conn, $_POST['state']);
    $pcode = mysqli_real_escape_string($connection->conn, $_POST['pcode']);
    $country = mysqli_real_escape_string($connection->conn, $_POST['country']);
    $phoneno = mysqli_real_escape_string($connection->conn, $_POST['phoneno']);

    /** Hashing password */
    $hashedpassword = Password_hash($password, PASSWORD_BCRYPT);
    
    /** Subject and body for sending mail */
    $subject = "My Page Account Confirmation";
    $body = "<p><strong>hey " . $fname . "</strong><br>We're ready to
    activate your account. All we need to do is make sure this is your email
    address.<br>Please click on the link below to veriry your account:<br><a href=
                    'http://localhost/php_oops/confirm.php?email="
                    . $email_address . "&token=$token'>Click Here</a><br>
                    If you didn't create a My Page account, just delete this
                    email and everything will go back to the way it was. </p>";
    if (send_mail($email_address, $fname, $subject, $body)) {
        $insert_data = array(
            'email' => $email_address,
            'password' => $hashedpassword,
            'fname' => $fname,
            'mname' => $mname,
            'lname' => $lname,
            'gender' => $_POST['gender'],
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'pincode' => $pcode,
            'country' => $country,
            'phone' => $phoneno,
            'verification_code' => $token,
            'is_email_confirmed' => 0
        );
        if ($connection->insert($insert_data)) {
            header('Location: login.php?registered=true');
            $connection->disconnect();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>registration page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/signup.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My registration page</a>
                </div>
                <div class="collapse navbar-collapse" id="navcollapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.php">
                                <span class="glyphicon glyphicon-log-in">
                                </span> Log In
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page">
            <form action="signup1.php" method="post">
                <div class="form-group">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 panel panel-default" id="left">
                                <div class="panel-body">
                                    <h3><b>Preffered User Information</b></h3>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li class="active" id="tab1">
                                            <a data-toggle="pill"
                                               href="#emailnpassword">
                                                    Email and Password
                                            </a>
                                        </li>
                                        <li id="tab2">
                                            <a data-toggle="pill"
                                               href="#acc_info" id="acc_tab">
                                                    Account Information
                                            </a>
                                        </li>
                                        <li id="tab3">
                                            <a data-toggle="pill" href="#add"
                                               id="contact">
                                                Contact Details
                                            </a>
                                        </li>
                                        <li id="tab4">
                                            <a data-toggle="pill" id="agree"
                                               href="#agreement">
                                                Agreement
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class=
                                    "col-md-6 tab-content panel panel-default"
                                 id="right">
                                <div id="emailnpassword"
                                     class="tab-pane fade in active">
                                    <div class="form-group">
                                    <label for="email">EMAIL:</label><br>
                                    <input type="text" class="form-control"
                                           id="email" name="email_address"
                                           placeholder="Email Address*"
                                           value=
                                        "<?php
                                        if(isset($_POST['email_address']))
                                            echo $_POST['email_address']; 
                                        ?>">
                                        <div class='label label-danger' 
                                             id="email_error"></div>
                                    <?php
                                        $validator->out_field_error(
                                            'email_address'
                                        );
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <label for="password">PASSWORD:</label><br>
                                    <input type="password" class="form-control"
                                        id= "password" name="password"
                                        placeholder= "Password*" value="<?php
                                            if(isset($_POST['password']))
                                                echo $_POST['password']; 
                                            ?>">
                                        <div class='label label-danger' 
                                             id="password_error"></div>
                                    <?php
                                        $validator->out_field_error('password');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <label for="cpassword">CONFIRM PASSWORD:
                                    </label><br>
                                    <input type="password" class="form-control"
                                        id= "cpassword" name="confirm_password"
                                        placeholder= "Confirm Password*" value=
                                        "<?php
                                        if(isset($_POST['confirm_password']))
                                            echo $_POST['confirm_password']; 
                                        ?>">
                                        <div class='label label-danger' 
                                             id="cpassword_error"></div>
                                    <?php
                                        $validator->out_field_error(
                                            'confirm_password'
                                        );
                                    ?>
                                    </div>
                                    <a data-toggle="pill" href="#acc_info"
                                       class="btn btn-primary" role="button"
                                       id="atag">
                                       Next<span class
                                       ="glyphicon glyphicon-chevron-right">
                                       </span>
                                   </a>
                                </div>
                                <div id="acc_info" class="tab-pane fade">
                                    <div class="form-group">
                                    <label>NAME:</label><br>
                                    <input type="text" class="form-control"
                                        name="fname" id="fname"
                                        placeholder="First Name*" value="<?php
                                            if(isset($_POST['fname']))
                                                echo $_POST['fname']; 
                                            ?>">
                                        <div class='label label-danger' 
                                             id="fname_error"></div>
                                    <?php
                                        $validator->out_field_error('fname');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="mname" placeholder="Middle Name"
                                        value="<?php
                                            if(isset($_POST['mname']))
                                                echo $_POST['mname']; 
                                            ?>" id="mname">
                                        <div class='label label-danger' 
                                             id="mname_error"></div>
                                    <?php
                                        $validator->out_field_error('mname');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="lname" placeholder="Last Name*"
                                        value="<?php
                                            if(isset($_POST['lname']))
                                                echo $_POST['lname']; 
                                            ?>" id="lname">
                                        <div class='label label-danger' 
                                             id="lname_error"></div>
                                    <?php
                                        $validator->out_field_error('lname');
                                    ?>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" 
                                                <?php
                                                    if (isset($_POST['gender'])
                                                        &&
                                                        $_POST['gender'] == "1"
                                                    )
                                                    echo "checked";
                                                ?>
                                                value="1" id="male">Male
                                        </label>
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="gender"
                                                <?php
                                                    if (isset($_POST['gender'])
                                                        &&
                                                        $_POST['gender'] == "2"
                                                    )
                                                    echo "checked";
                                                ?>
                                                    value="2" id="female">Female
                                        </label>
                                        <div class='label label-danger' 
                                             id="gender_error"></div>
                                    <?php
                                        $validator->out_field_error('gender');
                                    ?>
                                    </div>
                                    <a data-toggle="pill" href="#emailnpassword"
                                       class="btn btn-primary" role="button"
                                       id="previous1">
                                        <span class=
                                            "glyphicon glyphicon-chevron-left">
                                        </span>Previous
                                    </a>
                                    <a data-toggle="pill" href="#add" id="next2"
                                        class="btn btn-primary" role="button">
                                        Next<span class=
                                        "glyphicon glyphicon-chevron-right">
                                        </span>
                                    </a>
                                </div>
                                <div id="add" class="tab-pane fade">
                                    <div class="form-group">
                                    <label for="address">ADDRESS:</label><br>
                                    <input type="text" class="form-control"
                                        name="address" id="address"
                                        placeholder="Address*" value="<?php
                                            if(isset($_POST['address']))
                                                echo $_POST['address'];
                                            ?>">
                                        <div class='label label-danger' 
                                             id="address_error"></div>
                                    <?php
                                        $validator->out_field_error('address');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="city" placeholder="City*"
                                           id="city"
                                        value="<?php
                                            if(isset($_POST['city']))
                                                echo $_POST['city'];
                                            ?>">
                                        <div class='label label-danger' 
                                             id="city_error"></div>
                                    <?php
                                        $validator->out_field_error('city');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <select id="states" class="form-control" 
                                            name="state">
                                        <option value="" >
                                            State*
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'odisha') { 
                                                ?>selected="true" <?php }; ?>
                                                value="odisha">Odisha
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'westbengal') {
                                                ?>selected="true" <?php }; ?>
                                                value="westbengal">Westbengal
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'goa') {
                                                ?>selected="true" <?php }; ?>
                                                value="goa">Goa
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'maharashtra') {
                                                ?>selected="true" <?php }; ?>
                                                value="maharashtra">Maharashtra
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'delhi') { 
                                                ?>selected="true" <?php }; ?>
                                                value="delhi">Delhi
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'rajasthan') { 
                                                ?>selected="true" <?php }; ?>
                                                value="rajasthan">Rajasthan
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'karnataka') { 
                                                ?>selected="true" <?php }; ?>
                                                value="karnataka">Karnataka
                                        </option>
                                    </select>
                                        <div class='label label-danger' 
                                             id="state_error"></div>
                                    <?php
                                        $validator->out_field_error('state');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                           id="pincode"
                                        name="pcode" placeholder="Postal code*"
                                        value="<?php
                                            if(isset($_POST['pcode']))
                                                echo $_POST['pcode'];
                                            ?>">
                                        <div class='label label-danger' 
                                             id="pincode_error"></div>
                                    <?php
                                        $validator->out_field_error('pcode');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="country" placeholder="Country*"
                                           id="country"
                                        value="<?php
                                            if(isset($_POST['country']))
                                                echo $_POST['country'];
                                            ?>">
                                        <div class='label label-danger' 
                                             id="country_error"></div>
                                    <?php
                                        $validator->out_field_error('country');
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="phoneno" placeholder="Phone*"
                                           id="phoneno"
                                        value="<?php
                                            if(isset($_POST['phoneno']))
                                                echo $_POST['phoneno'];
                                            ?>">
                                        <div class='label label-danger' 
                                             id="phoneno_error"></div>
                                    <?php
                                        $validator->out_field_error('phoneno');
                                    ?>
                                    </div>
                                    <a data-toggle="pill" href="#acc_info"
                                       class="btn btn-primary" role="button"
                                       id="previous2">
                                        <span class=
                                        "glyphicon glyphicon-chevron-left">
                                        </span>Previous
                                    </a>
                                    <a data-toggle="pill" href="#agreement"
                                       class="btn btn-primary" role="button"
                                       id="next3">
                                        Next<span class=
                                            "glyphicon glyphicon-chevron-right">
                                            </span>
                                    </a>
                                </div>
                                <div id="agreement" class="tab-pane fade">
                                    <p>Membership to this portal is public. once
                                     your account information has been submitted
                                    , you will be immediatly granted access to
                                    the portal environment. Your information
                                    is for verification purpose and will not
                                    be shared by our platform. Be sure that
                                    above mentioned details are true as per your
                                     knowledge and the company will not be
                                    responsible for any kind of issues. Click on
                                    I agree to proceed. Check yours detail before
                                    submitting!</p>
                                    <div class="form-group">
                                    <input type="checkbox" id="i_agree"
                                    <?php
                                        if (isset($_POST['iagree']))
                                            echo "checked";
                                    ?> name="iagree"> I agree
                                    <?php
                                        $validator->out_field_error('iagree');
                                    ?>
                                    </div>
                                    <a data-toggle="pill" href="#add" 
                                       class="btn btn-primary" role="button"
                                       id="previous3">
                                        <span class=
                                        "glyphicon glyphicon-chevron-left">
                                        </span>Previous
                                    </a>
                                    <button type="submit" name="submit" id="submit"
                                            class="btn btn-success" disabled>
                                        Create account
                                    </button>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous">
        </script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"
                integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
                crossorigin="anonymous"></script>
        <script src="js/signup1.js"></script>
    </body>
</html>        