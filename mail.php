<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'vendor/autoload.php';
    require_once("resources/config.php");

    /**
     * Sending mail using smtp.
     *
     * @param $to
     * @param $name
     * @param $subject
     * @param $body
     *
     * @return bool
     */
    function send_mail($to, $name, $subject, $body)
    {
        global $MAIL_ID;
        global $MAIL_USERNAME;
        global $MAIL_PASSWORD;
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = $MAIL_ID;
            $mail->Password = $MAIL_PASSWORD;
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->setFrom($MAIL_ID, $MAIL_USERNAME);
            $mail->addAddress($to, $name);
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $body;
            $mail->AltBody = strip_tags($body);
            $mail->send();
            return true;
        } catch (Exception $e) {
            error_log('Message could not be sent. Mailer Error: '
                . $mail->ErrorInfo, 0);
        }
    }
