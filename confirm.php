<?php
require_once("resources/library/connection.php");
require_once("resources/config.php");

/**
 * Connecting to db.
 */
$connection = new Connection(
    $DB_HOST, 
    $DB_USERNAME, 
    $DB_PASSWORD,
    $DB_NAME
);
$email = $_GET['email'];
$token = $_GET['token'];
if ($connection->verify_email($email, $token)) {
    $connection->disconnect();
    header('Location: login.php');
}
