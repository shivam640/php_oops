<?php
require_once("resources/library/connection.php");
require_once("resources/config.php");

/**
 * Connecting to db.
 */
$connection = new Connection(
    $DB_HOST, 
    $DB_USERNAME, 
    $DB_PASSWORD, 
    $DB_NAME
);
if ($connection->check($_POST['email'])) {
    echo "Email already exist!";
} else {
    echo "";
}
