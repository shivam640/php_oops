<?php
require_once("resources/library/validator.php");
require_once("resources/library/connection.php");
require_once("resources/config.php");

session_start();
if (!isset($_SESSION['id'])){
    header('Location: login.php');
 }

/**
 * Connecting to db.
 */
$connection = new Connection(
        $DB_HOST, 
        $DB_USERNAME, 
        $DB_PASSWORD, 
        $DB_NAME
    );

/**
 * Validating form.
 *
 * @param $validator
 * @return bool
 */
function homepage_validation($validator)
{
    if ($_POST) {
        $validator->add_field('fname');
        $validator->add_rule_to_field('fname', array('empty'));
        $validator->add_rule_to_field('fname', array('name_format'));
        $validator->add_field('mname');
        $validator->add_rule_to_field('mname', array('name_format'));
        $validator->add_field('lname');
        $validator->add_rule_to_field('lname', array('empty'));
        $validator->add_rule_to_field('lname', array('name_format'));
        $validator->add_field('gender');
        $validator->add_rule_to_field('gender', array('select'));
        $validator->add_field('address');
        $validator->add_rule_to_field('address', array('empty'));
        $validator->add_field('city');
        $validator->add_rule_to_field('city', array('empty'));
        $validator->add_rule_to_field('city', array('name_format'));
        $validator->add_field('state');
        $validator->add_rule_to_field('state', array('select'));
        $validator->add_field('pcode');
        $validator->add_rule_to_field('pcode', array('empty'));
        $validator->add_rule_to_field('pcode', array('pcode'));
        $validator->add_field('country');
        $validator->add_rule_to_field('country', array('empty'));
        $validator->add_rule_to_field('country', array('name_format'));
        $validator->add_field('phoneno');
        $validator->add_rule_to_field('phoneno', array('empty'));
        $validator->add_rule_to_field('phoneno', array('phone'));
        if ($validator->form_valid()) {
            return true;
        }
    }
}

$data = $connection->get_data();
if($data['gender'] == 1) {
    $data['gender'] = 'male';
} else {
    $data['gender'] = 'female';
}

/**
 * For logout.
 */
if (isset($_POST['logout'])) {
        session_unset();
        session_destroy();
        header('Location: login.php');
        exit();
}

/**
 * Editing user details.
 */
if (isset($_POST['submit'])) {
    $validate = new Validator;
    if (homepage_validation($validate)) {
        if ($connection->change_data()) {
        } else {
            header('Location: error.php');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home page</title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/index.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"
                            data-toggle="collapse" data-target="#navcollapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> My Page</a>
                </div>
                <div  class="collapse navbar-collapse" id="navcollapse">
                <form class="navbar-form navbar-right" action="index.php"
                      method="post">
                    <button type="submit" name="logout" class="btn"><span
                            class="glyphicon glyphicon-log-in"></span> Log Out
                    </button>
                </form>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-3" id="left">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active" id="tab1">
                            <a data-toggle="pill" href="#home">User Information
                            </a>
                        </li>
                        <li id="tab2">
                            <a data-toggle="pill" href="#editpage">Edit Details
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9" id="right">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                                <h3>Basic Information</h3>
                                <p>Name:</p>
                                <h4><?php echo $data['fname'] . ' '
                                . $data['mname'] . ' ' . $data['lname'];?>
                                </h4>
                                <p>Email Address:</p>
                                    <h4><?php echo $data['email']; ?></h4>
                                <p>Gender:</p>
                                <h4><?php echo $data['gender']; ?></h4>
                                <h3>Contact Information</h3>
                                <p>address:</p>
                                <h4><?php echo $data['address'] .', ' 
                                . $data['city']; ?><br>
                                <?php echo $data['state'] . '-' 
                                . $data['pincode']; ?>
                                <br>
                                <?php echo $data['country']; ?>
                                </h4>
                                <p>Contact:</p>
                                <h4><?php echo $data['phone']; ?></h4>
                                <br>
                                <a data-toggle="pill" href="#editpage" 
                                   class="btn btn-primary" role="button"
                                   id="edit">
                                    <span class="glyphicon glyphicon-pencil">
                                    </span> Edit Details
                                </a>
                        </div>
                        <div id="editpage" class="tab-pane fade">
                            <form action="index.php" method="post">
                                <div class="form-group">
                                    <label>NAME:</label><br>
                                    <input type="text" class="form-control"
                                           name="fname" id="fname"
                                           placeholder="First Name*" 
                                           value="<?php
                                            if(isset($_POST['fname'])) {
                                                echo $_POST['fname']; 
                                            } else {
                                                echo $data['fname'];
                                            }
                                            ?>">
                                    <div class='label label-danger' 
                                         id="fname_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'fname'
                                            );
                                        }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="mname" 
                                           placeholder="Middle Name"
                                           id="mname"
                                           value="<?php
                                            if(isset($_POST['mname'])) {
                                                echo $_POST['mname'];
                                            } else {
                                                echo $data['mname'];
                                            }
                                            ?>">
                                    <div class='label label-danger' 
                                         id="mname_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'mname'
                                            );
                                        }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="lname" placeholder="Last Name*"
                                           id="lname"
                                           value="<?php
                                            if(isset($_POST['lname'])) {
                                                echo $_POST['lname'];
                                            } else {
                                                echo $data['lname'];
                                            }
                                            ?>">
                                    <div class='label label-danger' 
                                         id="lname_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'lname'
                                            );
                                        }
                                    ?>
                                </div>
                                <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" 
                                                <?php
                                                    if ((isset($_POST['gender'])
                                                        &&
                                                        $_POST['gender'] == "1")
                                                        || $data['gender'] == 
                                                        'male'
                                                    )
                                                    echo "checked";
                                                ?>
                                                value="1" id="male">Male
                                        </label>
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="gender"
                                                <?php
                                                    if ((isset($_POST['gender'])
                                                        &&
                                                        $_POST['gender'] == "2")
                                                        || $data['gender'] == 
                                                        'female'
                                                    )
                                                    echo "checked";
                                                ?>
                                                    value="2" id="female">Female
                                        </label>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'gender'
                                            );
                                        }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label for="address1">ADDRESS:</label><br>
                                    <input type="text" class="form-control"
                                           name="address" id="address"
                                           placeholder="Address*"
                                           value="<?php
                                            if(isset($_POST['address'])) {
                                                echo $_POST['address'];
                                            } else {
                                                echo $data['address'];
                                            }
                                            ?>">
                                    <div class='label label-danger' 
                                         id="address_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'address'
                                            );
                                        }
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="city" placeholder="City*"
                                           id="city"
                                           value="<?php
                                            if(isset($_POST['city'])) {
                                                echo $_POST['city'];
                                            } else {
                                                echo $data['city'];
                                            }
                                            ?>">
                                        <div class='label label-danger' 
                                             id="city_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error('city');
                                        }
                                    ?>
                                    </div>
                                <div class="form-group">
                                    <select id="states" class="form-control" 
                                            name="state" >
                                        <option value="" >
                                            State*
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'odisha' ||
                                                         $data['state'] ==
                                                         'odisha') { 
                                                ?>selected="true" <?php }; ?>
                                                value="odisha">Odisha
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'westbengal' ||
                                                         $data['state'] ==
                                                         'westbengal') {
                                                ?>selected="true" <?php }; ?>
                                                value="westbengal">Westbengal
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'goa' ||
                                                         $data['state'] ==
                                                         'goa') {
                                                ?>selected="true" <?php }; ?>
                                                value="goa">Goa
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'maharashtra' ||
                                                         $data['state'] ==
                                                         'maharashtra') {
                                                ?>selected="true" <?php }; ?>
                                                value="maharashtra">Maharashtra
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'delhi' ||
                                                         $data['state'] ==
                                                         'delhi') { 
                                                ?>selected="true" <?php }; ?>
                                                value="delhi">Delhi
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'rajasthan' ||
                                                         $data['state'] ==
                                                         'rajasthan') { 
                                                ?>selected="true" <?php }; ?>
                                                value="rajasthan">Rajasthan
                                        </option>
                                        <option <?php if ($_POST['state'] 
                                                          == 'karnataka' ||
                                                         $data['state'] ==
                                                         'karnataka') { 
                                                ?>selected="true" <?php }; ?>
                                                value="karnataka">Karnataka
                                        </option>
                                    </select>
                                    <div class='label label-danger' 
                                         id="states_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error('state');
                                        }
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="pcode" 
                                           placeholder="Postal code*"
                                           id="pincode"
                                           value="<?php
                                            if(isset($_POST['pcode'])) {
                                                echo $_POST['pcode'];
                                            } else {
                                                echo $data['pincode'];
                                            }
                                            ?>">
                                        <div class='label label-danger' 
                                             id="pincode_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error('pcode');
                                        }
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                        name="country" placeholder="Country*"
                                           id="country"
                                           value="<?php
                                            if(isset($_POST['country'])) {
                                                echo $_POST['country'];
                                            } else {
                                                echo $data['country'];
                                            }
                                            ?>">
                                        <div class='label label-danger' 
                                             id="country_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'country'
                                            );
                                        }
                                    ?>
                                    </div>
                                    <div class="form-group">
                                    <input type="text" class="form-control"
                                           name="phoneno" placeholder="Phone*"
                                           id="phone"
                                           value="<?php
                                            if(isset($_POST['phoneno'])) {
                                                echo $_POST['phoneno'];
                                            } else {
                                                echo $data['phone'];
                                            }
                                            ?>">
                                        <div class='label label-danger' 
                                             id="phone_error"></div>
                                    <?php
                                        if(isset($validate)) {
                                            $validate->out_field_error(
                                                'phoneno'
                                            );
                                        }
                                    ?>
                                    </div>
                                <br>
                                <button type="submit" name="submit"
                                        class="btn btn-success" id="submit">
                                    Submit
                                </button>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous">
        </script>
        <script src="js/index.js"></script>
    </body>
</html>