This README would normally document whatever steps are necessary to get your application up and running.

What is this repository for?
-----------------------------
Summary -> Done as a practice for learning PHP .

Version -> 1

How do I get set up?
--------------------
Summary of set up 
->Download the latest code of this project from the master branch of this project.
->Project code available at : https://shivam640@bitbucket.org/shivam640/php_oops.git

Configure as mentioned below 

Configuration
---------------
Step 1: Extract the downloaded .zip file
Step 2: Rename the folder and place it in the folder where your Apache server points to.
Step 3: Make a virtual host for this project and make it to point it to this projects root directory.
Step 4: Rename the config.example.php file to config.php and change the values of the constants according to the key names of the constants.
Step 5: Create a database for this project and put the same name in the configuration file.
Step 6: Goto migrations folder in this project's root directory and find 'user.sql' file.
Step 7: Export the file into the database you created for this project.
Step 8: Now, this project is configured. Open it in your favourite browser.

Coding guidelines
Coding guidelines file will be in the projects root directory.
(If not mentioned, assume to follow PSR-1 and PSR-2 coding guidelines.)
